package ann

import (
	"errors"
	"fmt"
)

type AnnIniter interface {
	Init(ann *Ann)
}

type AnnUpdater interface {
	Update(ann *Ann)
}

type Ann struct {
	tick       int
	historyLen int

	Input  Layer
	Hidden []Layer
	Output Layer

	Components []interface{}
	Updaters   []AnnUpdater
}

func NewAnn(
	inputs, outputs int, hidden []int,
	connectivity ConnectivityType,
	annComponents []interface{},
	inputComponents, outputComponents []interface{}, hiddenComponents [][]interface{},
) (ann *Ann) {
	ann = new(Ann)

	ann.Input = NewLayer("IN", inputs, inputComponents)
	ann.Output = NewLayer("ON", outputs, outputComponents)
	ann.Hidden = make([]Layer, len(hidden))
	if len(hiddenComponents) != len(hidden) {
		panic(errors.New("Len of hidden must coincide with number of hiddenComponents."))
	}
	for l := range ann.Hidden {
		ann.Hidden[l] = NewLayer(fmt.Sprintf("H%vN", l), hidden[l], hiddenComponents[l])
	}

	switch connectivity {
	default:
		panic(errors.New("Connectivity type is not recognized!"))
	case Connectivity.FeedForward:
		ann.connectFeedForward()
	case Connectivity.FullyConnected:
		ann.connectFully()
	}

	return
}

func (ann *Ann) Init() {
	for _, c := range ann.Components {
		if initer, ok := c.(AnnIniter); ok {
			initer.Init(ann)
		}
	}

	ann.Input.Init()
	for l := range ann.Hidden {
		ann.Hidden[l].Init()
	}
	ann.Output.Init()
}

func (ann *Ann) Tick(input []float64) (output []float64) {
	ann.tick++
	for i, stimulus := range input {
		ann.Input.Neurons[i].SI = stimulus
		ann.Input.Neurons[i].Val = stimulus
		ann.Input.Neurons[i].Update()
	}
	for l := 0; l < len(ann.Hidden); l++ {
		ann.Hidden[l].Update()
	}
	ann.Output.Update()
	output = ann.Output.Output()
	return
}

func (ann *Ann) Reset(input []float64) {
	for i, stimulus := range input {
		ann.Input.Neurons[i].Val = stimulus
	}

	for i := 0; i < 1+len(ann.Hidden); i++ {
		ann.Input.Update()

		for l := 0; l < len(ann.Hidden); l++ {
			ann.Hidden[l].Update()
		}

		ann.Output.Update()
	}

	ann.ForAllNeurons(func(_ *Ann, neuron *Neuron) {
		neuron.tick = ann.tick
	})
}

func (ann *Ann) Train(input, output []float64) {
	ann.ForAllNeurons(func(_ *Ann, neuron *Neuron) {
		neuron.SetHistoryLen(0)
	})

	for i := 0; i < 1+len(ann.Hidden); i++ {
		ann.tick++
		for i, stimulus := range input {
			ann.Input.Neurons[i].SI = stimulus
			ann.Input.Neurons[i].Val = stimulus
			ann.Input.Neurons[i].Update()
		}
		for l := 0; l < len(ann.Hidden); l++ {
			ann.Hidden[l].Update()
		}
		ann.Output.Train(output)
	}

	ann.ForAllNeurons(func(_ *Ann, neuron *Neuron) {
		neuron.tick = ann.tick
		neuron.SetHistoryLen(ann.historyLen)
	})
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------ Component functionss ---------------------------------------------
// -------------------------------------------------------------------------------------------------------

func (ann *Ann) AddAnnComponents(components ...interface{}) {
	ann.Components = append(ann.Components, components...)
	for _, c := range components {
		if updater, ok := c.(AnnUpdater); ok {
			ann.Updaters = append(ann.Updaters, updater)
		}
	}
}

func (ann *Ann) AddInputComponents(components ...interface{}) {
	ann.Components = append(ann.Components, components...)
	ann.Input.AddComponents(components)
}

func (ann *Ann) AddOutputComponents(components ...interface{}) {
	ann.Components = append(ann.Components, components...)
	ann.Output.AddComponents(components)
}

func (ann *Ann) AddHiddenComponentsToLayer(l int, components ...interface{}) {
	ann.Components = append(ann.Components, components...)
	ann.Hidden[l].AddComponents(components)
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- History functions -----------------------------------------------
// -------------------------------------------------------------------------------------------------------

func (ann *Ann) SetHistoryLen(historyLen int) {
	ann.historyLen = historyLen
	if ann.tick > historyLen {
		panic(errors.New("Trying to bound historyLen of ann but recorded history is already longer!"))
	}
	ann.ForAllNeurons(func(_ *Ann, n *Neuron) {
		n.SetHistoryLen(historyLen)
	})
}

func (ann *Ann) BoundHistoryLen() {
	ann.ForAllNeurons(func(_ *Ann, n *Neuron) {
		n.BoundHistoryLen()
	})
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------- Connect functions -----------------------------------------------
// -------------------------------------------------------------------------------------------------------

func (ann *Ann) connectInputToOutput() {
	ann.connectLayers(ann.Input.Neurons, ann.Output.Neurons)
}

func (ann *Ann) connectLayers(axons, dendrites []Neuron) {
	for d := 0; d < len(dendrites); d++ {
		dendrite := &dendrites[d]
		for a := 0; a < len(axons); a++ {
			axon := &axons[a]
			NewConnection(axon, dendrite, 0.0)
		}
	}
}

func (ann *Ann) connectToAll(dendrites []Neuron) {
	ann.connectLayers(ann.Input.Neurons, dendrites)
	for l := 0; l < len(ann.Hidden); l++ {
		ann.connectLayers(ann.Hidden[l].Neurons, dendrites)
	}
	ann.connectLayers(ann.Output.Neurons, dendrites)
}

func (ann *Ann) connectFeedForward() {
	axons := ann.Input.Neurons
	for l := 0; l < len(ann.Hidden); l++ {
		ann.connectLayers(axons, ann.Hidden[l].Neurons)
		axons = ann.Hidden[l].Neurons
	}
	ann.connectLayers(axons, ann.Output.Neurons)
}

func (ann *Ann) connectFully() {
	for l := 0; l < len(ann.Hidden); l++ {
		ann.connectToAll(ann.Hidden[l].Neurons)
	}
	ann.connectToAll(ann.Output.Neurons)
}

// -------------------------------------------------------------------------------------------------------
// -------------------------------- Public Helper functions ----------------------------------------------
// -------------------------------------------------------------------------------------------------------

func (ann *Ann) ForAllInputs(fn func(ann *Ann, neuron *Neuron)) {
	for i := range ann.Input.Neurons {
		fn(ann, &ann.Input.Neurons[i])
	}
}

func (ann *Ann) ForAllHiddenNeuronsInLayer(l int, fn func(ann *Ann, neuron *Neuron)) {
	ann.Hidden[l].ForAll(ann, fn)
}

func (ann *Ann) ForAllHiddenNeurons(fn func(ann *Ann, neuron *Neuron)) {
	for l := range ann.Hidden {
		ann.Hidden[l].ForAll(ann, fn)
	}
}

func (ann *Ann) ForAllOutputs(fn func(ann *Ann, neuron *Neuron)) {
	ann.Output.ForAll(ann, fn)
}

func (ann *Ann) ForAllNeurons(fn func(ann *Ann, neuron *Neuron)) {
	ann.ForAllInputs(fn)
	ann.ForAllHiddenNeurons(fn)
	ann.ForAllOutputs(fn)
}

func (ann *Ann) ForAllButInputs(fn func(ann *Ann, neuron *Neuron)) {
	ann.ForAllHiddenNeurons(fn)
	ann.ForAllOutputs(fn)
}

func (ann *Ann) String() (str string) {
	str = fmt.Sprintln(ann.Input)
	for l := range ann.Hidden {
		str = fmt.Sprintln(str, ann.Hidden[l])
	}
	str = fmt.Sprintln(str, ann.Output)
	return
}
