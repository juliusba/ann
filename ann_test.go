package ann

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
	"vecea"
	"vecmat"
)

func sigmoidComponents() (components []interface{}) {
	si := NewNU_SI_Regular()
	sigmoid := NewNU_Acti_Sigmoid()
	iw := NewNI_IW_Standard()
	components = []interface{}{
		iw,
		si,
		sigmoid,
	}
	return
}

func TestAnn(t *testing.T) {
	ann := NewAnn(8, 3, []int{6, 4}, Connectivity.FeedForward, nil, nil, nil, [][]interface{}{nil, nil})
	if len(ann.Input.Neurons) != 8 {
		t.Fail()
	} else if len(ann.Output.Neurons) != 3 {
		t.Fail()
	} else if len(ann.Hidden) != 2 {
		t.Fail()
	} else if len(ann.Hidden[0].Neurons) != 6 {
		t.Fail()
	} else if len(ann.Hidden[1].Neurons) != 4 {
		t.Fail()
	}
}

func TestConnection(t *testing.T) {
	var (
		inputs  = 8
		outputs = 3
		hidden  = []int{6, 4}
	)
	outputComponents := sigmoidComponents()
	hiddenComponents := [][]interface{}{outputComponents, outputComponents}
	ann := NewAnn(inputs, outputs, hidden, Connectivity.FeedForward,
		nil, nil, outputComponents, hiddenComponents)
	for i := range ann.Input.Neurons {
		if len(ann.Input.Neurons[i].Inputs) != 0 {
			t.Fail()
		}
	}
	prevLayerSize := inputs
	for l := range ann.Hidden {
		for h := range ann.Hidden[l].Neurons {
			if len(ann.Hidden[l].Neurons[h].Inputs) != prevLayerSize {
				t.Fail()
			}
		}
		prevLayerSize = len(ann.Hidden[l].Neurons)
	}
	for o := range ann.Output.Neurons {
		if len(ann.Output.Neurons[o].Inputs) != prevLayerSize {
			t.Fail()
		}
	}
}

func TestAnni(t *testing.T) {
	anni := NewAnnISigmoid(4, 2, []int{3, 2}, Connectivity.FeedForward)
	rand.Seed(time.Now().Unix())
	individ := vecea.NewIndivid(anni.Blueprint())
	individ.Randomize()

	ann := anni.Phenotype(individ).(*Ann)
	ann.Init()

	for i := 0; i < 50; i++ {
		output := ann.Tick(vecmat.RandVec(4))
		fmt.Println(output)
	}
}
