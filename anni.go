package ann

import (
	"errors"
	"fmt"
	"vecea"
)

type AnnI struct {
	Model Ann

	inputs  int
	outputs int
	hidden  []int

	connectivity ConnectivityType

	annComponents    []interface{}
	inputComponents  []interface{}
	outputComponents []interface{}
	hiddenComponents [][]interface{}

	createAnnFromPhenoVals func(phenoVals []float64, indexPtr *int) (ann *Ann)

	historyLen int

	neurons     int
	connections int
	blueprint   vecea.Blueprint
}

func NewAnnISigmoid(inputs, outputs int, hidden []int, connectivity ConnectivityType) (anni *AnnI) {
	var (
		iw               = NewNI_IW_Standard()
		si               = NewNU_SI_Regular()
		sigmoid          = NewNU_Acti_Sigmoid()
		annComponents    = []interface{}{}
		inputComponents  = []interface{}{}
		outputComponents = []interface{}{iw, si, sigmoid}
		hiddenComponents = make([][]interface{}, len(hidden))
	)

	for l := range hidden {
		hiddenComponents[l] = outputComponents
	}
	anni = NewAnnI(inputs, outputs, hidden, connectivity, annComponents, inputComponents, outputComponents, hiddenComponents)
	return
}

func NewAnnIBinProb(inputs, outputs int, hidden []int, connectivity ConnectivityType) (anni *AnnI) {
	var (
		iw               = NewNI_IW_Standard()
		si               = NewNU_SI_Regular()
		binProb          = NewNU_Acti_BinProb()
		annComponents    = []interface{}{}
		inputComponents  = []interface{}{binProb}
		outputComponents = []interface{}{iw, si, binProb}
		hiddenComponents = make([][]interface{}, len(hidden))
	)

	for l := range hidden {
		hiddenComponents[l] = outputComponents
	}
	anni = NewAnnI(inputs, outputs, hidden, connectivity, annComponents, inputComponents, outputComponents, hiddenComponents)
	return
}

func NewAnnI(inputs, outputs int, hidden []int, connectivity ConnectivityType,
	annComponents, inputComponents, outputComponents []interface{},
	hiddenComponents [][]interface{},
) (anni *AnnI) {
	anni = new(AnnI)
	anni.inputs = inputs
	anni.outputs = outputs
	anni.hidden = hidden

	anni.connectivity = connectivity

	anni.annComponents = annComponents
	anni.inputComponents = inputComponents
	anni.outputComponents = outputComponents
	anni.hiddenComponents = hiddenComponents
	anni.historyLen = 1000

	anni.init()
	return
}

func (anni *AnnI) init() {
	anni.Model = *NewAnn(anni.inputs, anni.outputs, anni.hidden, anni.connectivity,
		anni.annComponents, anni.inputComponents, anni.outputComponents, anni.hiddenComponents)

	// Create blueprint for genome

	getBPfunc := func(components []interface{}) (bpFunc func(_ *Ann, neuron *Neuron)) {
		neuronParams := make(map[NeuronKey]Param)
		inputParams := make(map[InputKey]Param)
		neuronBP := make(vecea.Blueprint, 0, 10)
		inputBP := make(vecea.Blueprint, 0, 10)
		for u := range components {
			if updater, ok := components[u].(NeuronParamer); ok {
				params := updater.NeuronParams()
				for _, param := range params {
					if original, ok := neuronParams[param.Key.(NeuronKey)]; ok {
						if !original.Equals(&param) {
							panic(errors.New(fmt.Sprintf("Conflicting params: (%v != %v)", original, param)))
						}
					} else {
						neuronBP = append(neuronBP, param.BP())
						neuronParams[param.Key.(NeuronKey)] = param
					}
				}
			}
			if updater, ok := components[u].(InputParamer); ok {
				params := updater.InputParams()
				for _, param := range params {
					if original, ok := inputParams[param.Key.(InputKey)]; ok {
						if !original.Equals(&param) {
							panic(errors.New("Conflicting params!"))
						}
					} else {
						inputBP = append(inputBP, param.BP())
						inputParams[param.Key.(InputKey)] = param
					}
				}
			}
		}
		if len(inputBP) > 0 {
			bpFunc = func(_ *Ann, neuron *Neuron) {
				for i := range neuronBP {
					neuronBP[i].Name = fmt.Sprintf("%s:%s", neuron.Id, neuronBP[i].Name)
				}
				anni.blueprint = append(anni.blueprint, neuronBP...)
				for _, input := range neuron.Inputs {
					for i := range inputBP {
						inputBP[i].Name = fmt.Sprintf("C(%s:%s):%s", input.AxonId, neuron.Id, inputBP[i].Name)
					}
					anni.blueprint = append(anni.blueprint, inputBP...)
				}
			}
		} else {
			bpFunc = func(_ *Ann, neuron *Neuron) {
				anni.blueprint = append(anni.blueprint, neuronBP...)
			}
		}
		return
	}

	anni.blueprint = make(vecea.Blueprint, 0)
	bpFunc := getBPfunc(anni.inputComponents)
	anni.Model.ForAllInputs(bpFunc)
	for l, components := range anni.hiddenComponents {
		bpFunc = getBPfunc(components)
		anni.Model.ForAllHiddenNeuronsInLayer(l, bpFunc)
	}
	bpFunc = getBPfunc(anni.outputComponents)
	anni.Model.ForAllOutputs(bpFunc)

	// Create phenotype function

	getPhenoFunc := func(components []interface{}) (
		phenoFunc func(_ *Ann, neuron *Neuron, phenoVals []float64, index *int),
	) {
		neuronParamKeys := make([]NeuronKey, 0, 10)
		inputParamKeys := make([]InputKey, 0, 5)
		for c := range components {
			if neuronParamer, ok := components[c].(NeuronParamer); ok {
				params := neuronParamer.NeuronParams()
				for _, param := range params {
					neuronParamKeys = append(neuronParamKeys, param.Key.(NeuronKey))
				}
			}
			if inputParamer, ok := components[c].(InputParamer); ok {
				params := inputParamer.InputParams()
				for _, param := range params {
					inputParamKeys = append(inputParamKeys, param.Key.(InputKey))
				}
			}
		}
		if len(inputParamKeys) > 0 {
			phenoFunc = func(_ *Ann, neuron *Neuron, phenoVals []float64, indexPtr *int) {
				for _, key := range neuronParamKeys {
					neuron.meta[key] = phenoVals[*indexPtr]
					*indexPtr++
				}
				for i := range neuron.Inputs {
					input := neuron.Inputs[i]
					for _, key := range inputParamKeys {
						input.meta[key] = phenoVals[*indexPtr]
						*indexPtr++
					}
				}
			}
		} else {
			phenoFunc = func(_ *Ann, neuron *Neuron, phenoVals []float64, indexPtr *int) {
				for _, key := range neuronParamKeys {
					neuron.meta[key] = phenoVals[*indexPtr]
					*indexPtr++
				}
			}
		}
		return
	}

	inputPhenoFunc := getPhenoFunc(anni.inputComponents)
	hiddenPhenoFuncs := make([]func(*Ann, *Neuron, []float64, *int), len(anni.hidden))
	for i := range hiddenPhenoFuncs {
		hiddenPhenoFuncs[i] = getPhenoFunc(anni.hiddenComponents[i])
	}
	outputPhenoFunc := getPhenoFunc(anni.outputComponents)

	anni.createAnnFromPhenoVals = func(phenoVals []float64, indexPtr *int) (ann *Ann) {
		ann = NewAnn(anni.inputs, anni.outputs, anni.hidden, anni.connectivity,
			anni.annComponents, anni.inputComponents, anni.outputComponents, anni.hiddenComponents)
		ann.ForAllInputs(func(_ *Ann, neuron *Neuron) {
			inputPhenoFunc(nil, neuron, phenoVals, indexPtr)
		})
		for l := range ann.Hidden {
			ann.ForAllHiddenNeuronsInLayer(l, func(_ *Ann, neuron *Neuron) {
				hiddenPhenoFuncs[l](nil, neuron, phenoVals, indexPtr)
			})
		}
		ann.ForAllOutputs(func(_ *Ann, neuron *Neuron) {
			outputPhenoFunc(nil, neuron, phenoVals, indexPtr)
		})

		ann.Init()
		return
	}
}

func (anni *AnnI) Inputs() int {
	return anni.inputs
}

func (anni *AnnI) Outputs() int {
	return anni.outputs
}

func (anni *AnnI) Blueprint() vecea.Blueprint {
	return anni.blueprint
}

func (anni *AnnI) GenomeLen() int {
	return len(anni.blueprint)
}

func (anni *AnnI) Phenotype(individ *vecea.Individ) interface{} {
	if ann, ok := individ.Meta("ann").(*Ann); ok {
		return ann
	}

	index := 0
	phenoVals := anni.blueprint.PhenoVals(individ.Genome())
	// fmt.Println(individ.Genome())
	ann := anni.createAnnFromPhenoVals(phenoVals, &index)

	ann.Init()

	individ.SetMeta("ann", ann, false)

	return ann
}

func (anni *AnnI) MetaData(ea *vecea.EA, individ *vecea.Individ, metaData map[string]interface{}) {
	metaData["ann"] = individ.Meta("ann")
	return
}

func (anni *AnnI) String() string {
	return fmt.Sprintf(
		"(Blueprinter)\t Anni (Inputs: %v, Outputs: %v, Hidden %v",
		anni.inputs, anni.outputs, anni.hidden)
}
