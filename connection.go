package ann

type InputParamer interface {
	InputParams() []Param
}

type InputVarer interface {
	InputVars() map[InputKey]float64
}

type InputPrereqer interface {
	InputPrereqs() []InputKey
}

type Connection struct {
	Weight float64
	AxonId string

	axon     *Neuron
	dendrite *Neuron

	meta map[InputKey]float64
}

func NewConnection(axon, dendrite *Neuron, weight float64) (conn *Connection) {
	conn = new(Connection)

	conn.Weight = weight
	conn.AxonId = axon.Id

	conn.axon = axon
	conn.dendrite = dendrite

	axon.AddOutput(conn)
	dendrite.AddInput(conn)

	conn.meta = make(map[InputKey]float64)

	return
}
