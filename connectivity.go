package ann

import "fmt"

type ConnectivityType int

var (
	Connectivity = struct {
		FeedForward    ConnectivityType
		FullyConnected ConnectivityType
	}{
		0,
		1,
	}
)

func NeuronInputTag(index int) string {
	return fmt.Sprintf("IN%v", index)
}

func NeuronOutputTag(index int) string {
	return fmt.Sprintf("ON%v", index)
}

func NeuronHiddenTag(layer, index int) string {
	return fmt.Sprintf("H%vN%v", layer, index)
}
