package ann

import (
	"errors"
	"fmt"
)

type Layer struct {
	Id string

	Neurons    []Neuron
	Components []interface{}
	Updaters   []NeuronUpdater
}

func NewLayer(id string, neurons int, components []interface{}) (l Layer) {
	l.Id = id
	l.Neurons = make([]Neuron, neurons)
	for i := 0; i < neurons; i++ {
		l.Neurons[i] = NewNeuron(fmt.Sprint(l.Id, i))
	}

	l.Components = components
	l.Updaters = make([]NeuronUpdater, 0, 5)
	for _, c := range components {
		if updater, ok := c.(NeuronUpdater); ok {
			l.Updaters = append(l.Updaters, updater)
		}
	}
	return
}

func (l *Layer) Init() {
	neuronVars := make(map[NeuronKey]float64)
	neuronParams := make(map[NeuronKey]float64)
	neuronIniters := make([]NeuronIniter, 0, 10)
	inputVars := make(map[InputKey]float64)
	inputParams := make(map[InputKey]float64)
	for _, c := range l.Components {
		// Check if prerequisites are met
		if pre, ok := c.(NeuronPrereqer); ok {
			prereqs := pre.NeuronPrereqs()
			for _, key := range prereqs {
				if _, ok := neuronVars[key]; !ok {
					panic(errors.New(fmt.Sprint("Neuron-prerequisite for component is not met: ", key)))
				}
			}
		}
		if pre, ok := c.(InputPrereqer); ok {
			prereqs := pre.InputPrereqs()
			for _, key := range prereqs {
				if _, ok := inputVars[key]; !ok {
					panic(errors.New(fmt.Sprint("Input-rerequisite for component is not met: ", key)))
				}
			}
		}

		// Register neuron- and input variables
		if varer, ok := c.(NeuronVarer); ok {
			newNeuronVars := varer.NeuronVars()
			for key, defaultVal := range newNeuronVars {
				neuronVars[key] = defaultVal
			}
		}
		if varer, ok := c.(InputVarer); ok {
			newInputVars := varer.InputVars()
			for key, defaultVal := range newInputVars {
				inputVars[key] = defaultVal
			}
		}

		// Register neuron parameters
		if paramer, ok := c.(NeuronParamer); ok {
			newNeuronParams := paramer.NeuronParams()
			for _, param := range newNeuronParams {
				neuronParams[param.Key.(NeuronKey)] = param.Default
			}
		}
		if paramer, ok := c.(InputParamer); ok {
			newInputParams := paramer.InputParams()
			for _, param := range newInputParams {
				inputParams[param.Key.(InputKey)] = param.Default
			}
		}

		// Register neuron initers
		if initer, ok := c.(NeuronIniter); ok {
			neuronIniters = append(neuronIniters, initer)
		}
	}

	// Initialize variables and parameters and call initers on each neuron
	for n := range l.Neurons {
		for key := range neuronVars {
			l.Neurons[n].meta[key] = 0.0
		}

		for key, defaultVal := range neuronParams {
			if _, ok := l.Neurons[n].meta[key]; !ok {
				l.Neurons[n].meta[key] = defaultVal
			}
		}
		for i := range l.Neurons[n].Inputs {
			input := l.Neurons[n].Inputs[i]
			for key := range inputVars {
				input.meta[key] = 0.0
			}

			for key, defaultVal := range inputParams {
				if _, ok := input.meta[key]; !ok {
					input.meta[key] = defaultVal
				}
			}
		}

		for _, initer := range neuronIniters {
			initer.Init(&l.Neurons[n])
		}
	}
}

func (l *Layer) Update() {
	for _, updater := range l.Updaters {
		for i := range l.Neurons {
			updater.Update(&l.Neurons[i])
		}
	}
	for i := range l.Neurons {
		l.Neurons[i].Update()
	}
}

func (l *Layer) Train(output []float64) {
	for i := range l.Neurons {
		l.Neurons[i].Val = output[i]
	}
	for _, comp := range l.Components {
		if trainer, ok := comp.(NeuronTrainer); ok {
			for i := range l.Neurons {
				trainer.Train(&l.Neurons[i])
			}
		}
	}
	for i := range l.Neurons {
		l.Neurons[i].Update()
	}
}

func (outputLayer *Layer) ConnectTo(inputLayer *Layer) {
	for o := 0; o < len(outputLayer.Neurons); o++ {
		dendrite := &outputLayer.Neurons[o]
		for i := 0; i < len(inputLayer.Neurons); i++ {
			axon := &inputLayer.Neurons[i]
			NewConnection(axon, dendrite, 0.0)
		}
	}
}

func (l *Layer) Output() (output []float64) {
	output = make([]float64, len(l.Neurons))
	for i := 0; i < len(l.Neurons); i++ {
		output[i] = l.Neurons[i].Val
	}
	return
}

func (l *Layer) Change() (change float64) {
	for i := 0; i < len(l.Neurons); i++ {
		change += l.Neurons[i].Change()
	}
	return
}

func (l *Layer) AddComponents(components ...interface{}) {
	l.Components = append(l.Components, components)
	for _, c := range components {
		if updater, ok := c.(NeuronUpdater); ok {
			l.Updaters = append(l.Updaters, updater)
		}
	}
}

func (l *Layer) ForAll(ann *Ann, fn func(ann *Ann, n *Neuron)) {
	for i := range l.Neurons {
		fn(ann, &l.Neurons[i])
	}
}

func (l *Layer) String() (str string) {
	str = fmt.Sprintf("%s: [", l.Id)
	for i := 0; i < len(l.Neurons); i++ {
		str = fmt.Sprint(str, " ", l.Neurons[i].Val)
	}
	str = fmt.Sprintf("%s\n", str)
	return
}
