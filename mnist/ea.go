package mnist

import (
	"fmt"
	"vecea"
)

type closenessTester struct {
	mnist *MNIST
}

func NewClosenessTester(mnist *MNIST) (tester *closenessTester) {
	tester = new(closenessTester)
	tester.mnist = mnist
	return
}

func (tester *closenessTester) TestIndivid(individ *vecea.Individ, phenotype interface{}) (score float64) {
	outputs := individ.Meta("outputs").([][]float64)
	for i := range outputs {
		label := tester.mnist.Labels[i]
		output := outputs[i]

		correctOutput := output[label]
		score += correctOutput
		for _, o := range output {
			if o > correctOutput {
				score -= output[label]
			}
		}
	}

	individ.SetMeta("outputs", nil, false)

	return
}

func (_ *closenessTester) TestName() (name string) {
	return "closeness"
}

func (_ *closenessTester) TestSpecs() (minimize, logarithmic bool) {
	minimize = false
	logarithmic = false
	return
}

func (_ *closenessTester) TestGoal() (goal float64, hasGoal bool) {
	hasGoal = false
	return
}

func (_ *closenessTester) String() string {
	return fmt.Sprintf("(Tester)\t Closeness")
}
