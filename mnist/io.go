package mnist

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"math/rand"
	"os"
)

const (
	_labelCount      = 10
	_trainingSamples = 60000
	_testSamples     = 10000
	_rows, _cols     = 28, 28

	_trainingLabelsFileName = "data/train-labels.idx1-ubyte"
	_trainingImagesFileName = "data/train-images.idx3-ubyte"
	_testLabelsFileName     = "data/t10k-labels.idx1-ubyte"
	_testImagesFileName     = "data/t10k-images.idx3-ubyte"
)

var _sets = make(map[string]struct {
	Images [][]float64
	Labels []uint8
})

func GetSet(samples, pixelSize int, blur bool) (images [][]float64, labels []uint8) {
	str := fmt.Sprintf("-%v-%t", pixelSize, blur)

	// See if samples is ready in memory
	if set, ok := _sets[fmt.Sprint(samples, str)]; ok {
		return set.Images, set.Labels
	}

	file, err := os.Open(fmt.Sprintf("data/images%s", str))
	// See if requested samples are generated from originals
	if err != nil {
		// Generate requested samples from original file
		_createSet(pixelSize, blur)
		file, err = os.Open(fmt.Sprintf("data/images%s", str))
		if err != nil {
			fmt.Println("Something went wrong during creation of imagefile!")
			panic(err)
		}
	}

	defer func() {
		if err := file.Close(); err != nil {
			panic(err)
		}
	}()

	index := rand.Intn(_trainingSamples - samples)

	images = _readImages(file, index, samples, pixelSize)
	labels = _readLabels(index, samples)
	_sets[str] = struct {
		Images [][]float64
		Labels []uint8
	}{
		images,
		labels,
	}

	return
}

func _readLabels(index, samples int) (labels []uint8) {
	labels = make([]uint8, samples)

	file, err := os.Open(_trainingLabelsFileName)
	_checkError(err)

	bufSize := samples
	firstByte := int64(8 + index)
	buf := make([]byte, bufSize)
	_, err = file.ReadAt(buf, firstByte)
	_checkError(err)
	buffer := bytes.NewBuffer(buf)
	binary.Read(buffer, binary.BigEndian, &labels)
	return
}

func _readImages(file *os.File, index, samples, pixelSize int) (images [][]float64) {
	images = make([][]float64, samples)
	for i := range images {
		rows, cols := _imageSize(pixelSize)
		images[i] = make([]float64, rows*cols)
	}

	imageSize := binary.Size(images[0])
	bufSize := imageSize * samples
	firstByte := int64(imageSize * index)
	buf := make([]byte, bufSize)
	_, err := file.ReadAt(buf, firstByte)
	_checkError(err)
	buffer := bytes.NewBuffer(buf)

	for i := range images {
		binary.Read(buffer, binary.BigEndian, &images[i])
	}

	return
}

// TODO: Padd to fit with pixelsizes other than 1,2,4 and 7
func _createSet(pixelSize int, blur bool) {
	originalImages := _loadOriginalImages(false)

	images := make([][]float64, len(originalImages))
	for i := range originalImages {
		rows, cols := _imageSize(pixelSize)
		image := make([]float64, rows*cols)
		originalImage := originalImages[i]
		index := 0
		for r := 0; r < rows; r++ {
			for c := 0; c < cols; c++ {
				start := (r*_cols + c) * pixelSize
				end := start + pixelSize
				for k := 0; k < pixelSize; k++ {
					for j := start; j < end; j++ {
						image[index] += originalImage[j]
					}
					start += _cols
					end += _cols
				}
				image[index] /= float64(pixelSize * pixelSize)
				index++
			}
		}

		if blur {
			prevImage := image
			image = make([]float64, len(prevImage)/2)
			prevCols := cols
			cols = prevCols / 2
			index = 0
			prevIndex := 0
			for r := 1; r < rows-1; r++ {
				for c := 1; c < cols-1; c++ {
					image[index] += prevImage[prevIndex]
					image[index] += prevImage[prevIndex-1] * 0.5
					image[index] += prevImage[prevIndex+1] * 0.5
					image[index] += prevImage[prevIndex-prevCols] * 0.5
					image[index] += prevImage[prevIndex+prevCols] * 0.5
					image[index] /= 3
					index++
					prevIndex += 2
				}
				index += 2
				prevIndex += 4
				if r%2 == 1 {
					prevIndex++
				} else {
					prevIndex--
				}
			}

			// Top and bottom line
			for t, b := 1, 1+(rows-1)*cols; t < cols-1; t, b = t+1, b+1 {
				image[t] += prevImage[t]
				image[t] += prevImage[t-1] * 0.5
				image[t] += prevImage[t+1] * 0.5
				image[t] += prevImage[t+prevCols] * 0.5
				image[t] /= 2.5

				image[b] += prevImage[b]
				image[b] += prevImage[b-1] * 0.5
				image[b] += prevImage[b+1] * 0.5
				image[b] += prevImage[b-prevCols] * 0.5
				image[b] /= 2.5
			}

			// Left and right line
			lastL := rows * (rows - 1)
			for l, r := cols, 2*cols-1; l < lastL; l, r = l+cols, r+cols {
				image[l] += prevImage[l]
				image[l] += prevImage[l+1] * 0.5
				image[l] += prevImage[l+prevCols] * 0.5
				image[l] += prevImage[l-prevCols] * 0.5
				image[l] /= 2.5

				image[r] += prevImage[r]
				image[r] += prevImage[r-1] * 0.5
				image[r] += prevImage[r+prevCols] * 0.5
				image[r] += prevImage[r-prevCols] * 0.5
				image[r] /= 2.5
			}

			// Corners
			image[0] = 0.5*prevImage[0] +
				0.25*prevImage[1] + 0.25*prevImage[prevCols]
			image[cols-1] = 0.5*prevImage[prevCols-1] +
				0.25*prevImage[prevCols-2] + 0.25*prevImage[prevCols*2-1]
			image[(rows-1)*cols] = 0.5*prevImage[(rows-1)*prevCols] +
				0.25*prevImage[(rows-1)*cols+1] + 0.25*prevImage[(rows-2)*prevCols]
			image[rows*cols-1] = 0.5*prevImage[rows*prevCols-1] +
				0.25*prevImage[(rows-1)*prevCols] + 0.25*prevImage[rows*prevCols-2]

		}
		images[i] = image
	}

	file, err := os.Create(fmt.Sprintf("data/images-%v-%t", pixelSize, blur))
	_checkError(err)

	for i := range images {
		err = binary.Write(file, binary.BigEndian, images[i])
		_checkError(err)
	}

	err = file.Close()
	_checkError(err)
}

func _loadOriginalLabels(test bool) (labels []uint8) {
	var fileName string
	if test {
		fileName = _testLabelsFileName
	} else {
		fileName = _trainingLabelsFileName
	}

	file, err := os.Open(fileName)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := file.Close(); err != nil {
			panic(err)
		}
	}()

	_readInt32(file) // magic number
	labelCount := int(_readInt32(file))
	if test && labelCount != _testSamples {
		panic(errors.New(fmt.Sprintf("Constants _testSamples are incorrect! %v != %v", labelCount, _testSamples)))
	} else if !test && labelCount != _trainingSamples {
		panic(errors.New(fmt.Sprintf("Constants _trainingSamples are incorrect! %v != %v", labelCount, _trainingSamples)))
	}
	labels = make([]uint8, labelCount)
	for i := 0; i < labelCount; i++ {
		labels[i] = _readUint8(file)
		fmt.Printf("\rReading training set labels from %s %v%s  ", fileName, int(float32(i)/float32(labelCount)*100), "%")
	}
	fmt.Printf("\rReading training set labels from %s      \n", fileName)
	return
}

func _loadOriginalImages(test bool) (images [][]float64) {
	var fileName string
	if test {
		fileName = _testImagesFileName
	} else {
		fileName = _trainingImagesFileName
	}

	file, err := os.Open(fileName)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := file.Close(); err != nil {
			panic(err)
		}
	}()

	_readInt32(file) // magic number
	imageCount := int(_readInt32(file))
	if test && imageCount != _testSamples {
		panic(errors.New(fmt.Sprintf("Constants _testSamples are incorrect! %v != %v", imageCount, _testSamples)))
	} else if !test && imageCount != _trainingSamples {
		panic(errors.New(fmt.Sprintf("Constants _trainingSamples are incorrect! %v != %v", imageCount, _trainingSamples)))
	}

	rows, cols := int(_readInt32(file)), int(_readInt32(file))
	if rows != _rows || cols != _cols {
		panic(errors.New(fmt.Sprintf("Constants _rows and _cols are incorrect! (%v, %v) != (%v, %v)", rows, cols, _rows, _cols)))
	}

	images = make([][]float64, imageCount)
	for i := 0; i < imageCount; i++ {
		images[i] = _readRawImage(file, rows, cols)
		fmt.Printf("\rReading images from file %s %v%s  ", fileName, int(float32(i)/float32(imageCount)*100), "%")
	}
	fmt.Printf("\rReading images from file %s       \n", fileName)
	return
}

func _readInt32(file *os.File) (ret int32) {
	err := binary.Read(file, binary.BigEndian, &ret)
	if err != nil {
		fmt.Println(err)
	}
	return
}

func _readUint8(file *os.File) (ret uint8) {
	err := binary.Read(file, binary.BigEndian, &ret)
	if err != nil {
		fmt.Println(err)
	}
	return
}

func _readRawImage(file *os.File, rows, cols int) (image []float64) {
	size := rows * cols
	image = make([]float64, size)
	for i := 0; i < size; i++ {
		image[i] = float64(_readUint8(file)) / 256.0
	}
	return
}

func _checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func _imageSize(pixelSize int) (rows, cols int) {
	return _rows / pixelSize, _cols / pixelSize
}
