package mnist

import (
	"ann"
	"fmt"
	"log"
	"math"
	"reflect"
	"vecea"
)

const (
	testSamples = 100
)

type MNIST struct {
	Samples        int
	PixelSize      int
	Blur           bool
	TrainIndivids  bool
	Walk           bool
	WalkViewRadius float64
	WalkDuration   int

	Images     [][]float64
	Rows, Cols int
	Labels     []uint8
	LabelCount []float64
}

func NewMNIST(samples, pixelSize int, blur bool) (m *MNIST, inputs, outputs int) {
	m = new(MNIST)
	m.Samples = samples
	m.PixelSize = pixelSize
	m.Blur = blur

	m.Images, m.Labels = GetSet(samples+testSamples, pixelSize, blur)
	m.LabelCount = make([]float64, _labelCount)
	for i := 0; i < m.Samples; i++ {
		m.LabelCount[m.Labels[i]]++
	}
	m.Rows, m.Cols = _imageSize(pixelSize)

	inputs = m.Rows * m.Cols
	outputs = _labelCount
	return
}

func NewMNISTwalk(samples, pixelSize int, blur bool, viewRadius float64, duration int) (m *MNIST, inputs, outputs int) {
	m = new(MNIST)
	m.Samples = samples
	m.PixelSize = pixelSize
	m.Blur = blur
	m.Walk = true
	m.WalkViewRadius = viewRadius
	m.WalkDuration = duration

	m.Images, m.Labels = GetSet(samples+testSamples, pixelSize, blur)
	m.LabelCount = make([]float64, _labelCount)
	for i := 0; i < m.Samples; i++ {
		m.LabelCount[m.Labels[i]]++
	}
	m.Rows, m.Cols = _imageSize(pixelSize)

	inputs = int(math.Pow(2*viewRadius, 2))
	outputs = _labelCount + len(cmds)
	return
}

func (mnist *MNIST) Train(ann *ann.Ann) {
	for i := mnist.Samples; i < len(mnist.Images); i++ {
		img := mnist.Images[i]
		label := mnist.Labels[i]
		output := make([]float64, _labelCount)
		output[int(label)] = 1.0
		// ann.Reset(img)
		ann.Train(img, output)
		ann.Train(img, output)
		ann.Train(img, output)
	}
	return
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------ EA-related -------------------------------------------------------
// -------------------------------------------------------------------------------------------------------

func (mnist *MNIST) TestIndivid(individ *vecea.Individ, phenotype interface{}) (score float64) {
	ann := phenotype.(*ann.Ann)

	if mnist.TrainIndivids {
		mnist.Train(ann)
	}

	classifications := make([]uint8, mnist.Samples)
	outputs := make([][]float64, 0, mnist.Samples)
	correctGuesses := make([]float64, _labelCount)
	fails := 0
	for i := 0; i < mnist.Samples && fails < 50; i++ {
		img := mnist.Images[i]
		label := mnist.Labels[i]
		if mnist.Walk {
			walk := NewWalk(img, float64(mnist.Rows), float64(mnist.Cols), mnist.WalkViewRadius)
			walk.TestAnn(ann, mnist.WalkDuration)
		} else {
			// ann.Reset(img)
			ann.Tick(img)
			ann.Tick(img)
			ann.Tick(img)
		}
		output := ann.Output.Output()[:10]
		var (
			classification uint8
			max            float64
		)
		for i, out := range output {
			if out > max {
				max = out
				classification = uint8(i)
			}
		}
		classifications[i] = classification
		outputs = append(outputs, output)
		if classification == label {
			correctGuesses[label]++
		} else {
			fails++
		}
	}
	for i := range correctGuesses {
		score += correctGuesses[i]
	}

	individ.SetMeta("error", 1-score/(score+50), true)
	individ.SetMeta("classifications", classifications, false)
	individ.SetMeta("outputs", outputs, false)
	return
}

func (mnist *MNIST) RunFinalTest(ann *ann.Ann) (err float64) {
	correctClassifications := 0.0
	for i := mnist.Samples; i < len(mnist.Images); i++ {
		img := mnist.Images[i]
		label := mnist.Labels[i]
		if mnist.Walk {
			walk := NewWalk(img, float64(mnist.Rows), float64(mnist.Cols), mnist.WalkViewRadius)
			walk.TestAnn(ann, mnist.WalkDuration)
		} else {
			// ann.Reset(img)
			ann.Tick(img)
			ann.Tick(img)
			ann.Tick(img)
		}
		output := ann.Output.Output()[:10]
		var (
			classification uint8
			max            float64
		)
		for i, out := range output {
			if out > max {
				max = out
				classification = uint8(i)
			}
		}
		if classification == label {
			correctClassifications++
		}
	}
	err = 1.0 - (correctClassifications / float64(testSamples))
	return
}

func (mnist *MNIST) TestName() (name string) {
	return "mnist"
}

func (mnist *MNIST) TestSpecs() (minimize, logarithmic bool) {
	minimize = false
	logarithmic = false
	return
}

func (mnist *MNIST) TestGoal() (goal float64, hasGoal bool) {
	hasGoal = false
	return
}

func (mnist *MNIST) Init(ea *vecea.EA) {
	anni, ok := ea.Blueprinter.(*ann.AnnI)
	rows, cols := mnist.Rows, mnist.Cols
	if !ok {
		log.Fatalf("ERROR (mnist.Init): Blueprinter must be AnnI! (Is %v)\n", reflect.TypeOf(ea.Blueprinter).Name())
	} else if mnist.Walk {
		if anni.Inputs() != int(math.Pow(2*mnist.WalkViewRadius, 2)) {
			log.Fatalf("ERROR (mnist.Init): Ann must have %v inputs! (Has %v)\n", math.Pow(2*mnist.WalkViewRadius, 2), anni.Inputs())
		} else if anni.Outputs() != _labelCount+len(cmds) {
			log.Fatalf("ERROR (mnist.Init): Ann must have %v outputs! (Has %v)\n", _labelCount+len(cmds), anni.Outputs())
		}
	} else {
		if anni.Inputs() != rows*cols {
			log.Fatalf("ERROR (mnist.Init): Ann must have %v*%v=%v inputs! (Has %v)\n", rows, cols, rows*cols, anni.Inputs())
		} else if anni.Outputs() != _labelCount {
			log.Fatalf("ERROR (mnist.Init): Ann must have %v outputs! (Has %v)\n", _labelCount, anni.Outputs())
		}
	}
}

func (mnist *MNIST) String() string {
	return fmt.Sprintf("(Tester)\t MNIST (Samples: %v, PixelSize: %v, Blur: %t)", mnist.Samples, mnist.PixelSize, mnist.Blur)
}

func (mnist *MNIST) MetaData(ea *vecea.EA, individ *vecea.Individ, metaData map[string]interface{}) {
	metaData["mnist"] = struct {
		Images          [][]float64
		Labels          []uint8
		Classifications []uint8
		Outputs         [][]float64
		Rows, Cols      int
	}{
		mnist.Images,
		mnist.Labels,
		individ.Meta("classifications").([]uint8),
		individ.Meta("outputs").([][]float64),
		mnist.Rows, mnist.Cols,
	}
}
