package mnist

import (
	"ann"
	"fmt"
	"runtime"
	"testing"
	"vecea"
)

var (
	samples    = 400
	pixelSize  = 2
	blur       = false
	viewRadius = 3.5
	duration   = 25
)

func TestSetGenerator(t *testing.T) {
	GetSet(samples, pixelSize, blur)
}

// func TestMNISTWalk(t *testing.T) {
// 	runtime.GOMAXPROCS(runtime.NumCPU())
// 	mnist, inputs, outputs := NewMNISTwalk(samples, pixelSize, blur, viewRadius, duration)
// 	anni := ann.NewAnnISigmoid(inputs, outputs, []int{20}, ann.Connectivity.FullyConnected)
// 	ea := vecea.NewEADelta(50, anni)

// 	ea.AddTesters(mnist)
// 	ea.AddTesters(ann.NewAnnTesterNeuronSignificanceN(20), 0.2)
// 	ea.AddTesters(NewClosenessTester(mnist), 0.2)
// 	ea.Init()
// 	ea.BoundAncestors()
// 	for i := 0; i < 100; i++ {
// 		stats, _ := ea.RunGenerationStep()
// 		var memstats runtime.MemStats
// 		runtime.ReadMemStats(&memstats)
// 		fmt.Printf("\rGen: %v, Error: %v, Memory: %v PopSize: %v    ",
// 			stats.Generation, ea.IndividBest().Meta("error"), memstats.Alloc/1048576, ea.PopulationSize())
// 	}

// 	// Test the best individ!!!!!

// 	fmt.Println("Final score:", mnist.RunFinalTest(ea.Phenotype(ea.IndividBest()).(*ann.Ann)))
// 	fmt.Println()
// 	fmt.Println()
// }

// func TestMNIST(t *testing.T) {
// 	runtime.GOMAXPROCS(runtime.NumCPU())
// 	mnist, inputs, outputs := NewMNIST(samples, pixelSize, blur)
// 	anni := ann.NewAnnISigmoid(inputs, outputs, []int{}, ann.Connectivity.FullyConnected)
// 	ea := vecea.NewEADelta(100, anni)

// 	ea.AddTesters(mnist)
// 	// ea.AddTesters(ann.NewAnnTesterNeuronSignificanceN(20), 0.2)
// 	ea.AddTesters(NewClosenessTester(mnist), 0.2)
// 	ea.Init()
// 	done := false
// 	for i := 0; i < 600 && !done; i++ {
// 		done = ea.RunGenerationStep()
// 		var memstats runtime.MemStats
// 		runtime.ReadMemStats(&memstats)
// 		fmt.Printf("\rGen: %v, Error: %v, Memory: %v PopSize: %v    ",
// 			i, ea.IndividBest().Meta("error"), memstats.Alloc/1048576, len(ea.Population))

// 		if i%50 == 0 {
// 			fmt.Println("Final score:", mnist.RunFinalTest(ea.Phenotype(ea.IndividBest()).(*ann.Ann)))
// 		}
// 	}
// 	fmt.Println("Final score:", mnist.RunFinalTest(ea.Phenotype(ea.IndividBest()).(*ann.Ann)))
// 	fmt.Println()
// 	fmt.Println()
// }

func TestTrainMNIST(t *testing.T) {
	runtime.GOMAXPROCS(runtime.NumCPU())
	mnist, inputs, outputs := NewMNIST(samples, pixelSize, blur)
	anni := ann.NewAnnIBinProb(inputs, outputs, []int{100}, ann.Connectivity.FeedForward)
	ea := vecea.NewEADelta(5, anni)
	ea.AddTesters(mnist)
	ea.Init()
	done := false
	for i := 0; i < 600 && !done; i++ {
		done = ea.RunGenerationStep()
		var memstats runtime.MemStats
		runtime.ReadMemStats(&memstats)
		fmt.Printf("\rGen: %v, Error: %v, Memory: %v PopSize: %v    ",
			i, ea.IndividBest().Meta("error"), memstats.Alloc/1048576, len(ea.Population))

		if i%50 == 0 {
			fmt.Println("Final score:", mnist.RunFinalTest(ea.Phenotype(ea.IndividBest()).(*ann.Ann)))
		}
	}
	fmt.Println("Final score:", mnist.RunFinalTest(ea.Phenotype(ea.IndividBest()).(*ann.Ann)))
	fmt.Println()
	fmt.Println()
}
