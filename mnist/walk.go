package mnist

import (
	"ann"
	"math"
	"math/rand"
)

type Walk struct {
	x, y     float64
	angle    float64
	cos, sin float64

	// The image
	img        []float64
	rows, cols float64

	// The view of the agent
	view       []float64
	viewRadius float64

	_render bool
}

func NewWalk(img []float64, rows, cols, viewRadius float64) (w *Walk) {
	w = new(Walk)
	w.img = img
	w.rows = rows
	w.cols = cols
	w.viewRadius = viewRadius

	w.y = math.Floor(rows / 2)
	w.x = math.Floor(cols / 2)
	w.cos = 1.0
	w.view = make([]float64, int(math.Pow(viewRadius*2, 2)))
	w._render = true
	return
}

type cmd struct{ x, y, angle float64 }

var (
	cmds map[int]cmd = map[int]cmd{
		10: cmd{0, 0, 0},
		11: cmd{1, 0, 0},
		12: cmd{-1, 0, 0},
		13: cmd{0, 1, 0},
		14: cmd{0, -1, 0},
		15: cmd{0, 0, 0.1},
		16: cmd{0, 0, -0.1},
	}
)

func (w *Walk) TestAnn(ann *ann.Ann, duration int) {
	for i := 0; i < duration; i++ {
		output := ann.Tick(w.View())
		max := -1.0
		cmdIndexes := make([]int, 1, len(cmds))
		for i := 10; i < 10+len(cmds); i++ {
			if output[i] > max {
				max = output[i]
				cmdIndexes[0] = i
				cmdIndexes = cmdIndexes[:1]
			} else if output[i] == max {
				cmdIndexes = append(cmdIndexes, i)
			}
		}
		cmdIndex := cmdIndexes[0]
		if len(cmdIndexes) > 1 {
			cmdIndex = cmdIndexes[rand.Intn(len(cmdIndexes))]
		}
		cmd := cmds[cmdIndex]
		w.Turn(cmd.angle)
		w.Walk(cmd.x, cmd.y)
	}
}

func (w *Walk) Walk(y, x float64) {
	if y != 0 || x != 0 {
		w.x += x*w.cos - y*w.sin
		w.y += y*w.cos + x*w.sin
		w._render = true
	}
}

func (w *Walk) Turn(angle float64) {
	if angle != 0 {
		w.angle += angle
		w.cos = math.Cos(w.angle)
		w.sin = math.Sin(w.angle)
		w._render = true
	}
}

func (w *Walk) Zoom(ratio float64) {
	// TODO
}

func (w *Walk) View() []float64 {
	if w._render {
		w.render()
	}

	return w.view
}

func (w *Walk) render() {
	w._render = false
	viewIndex := 0

	for y := -w.viewRadius + 0.5; y < w.viewRadius; y++ {
		for x := -w.viewRadius + 0.5; x < w.viewRadius; x++ {
			row := w.y + (x*w.sin + y*w.cos)
			rowLow := math.Floor(row)
			rowHigh := math.Ceil(row)
			rowViggle := row - rowLow

			col := w.x + (x*w.cos - y*w.sin)
			colLow := math.Floor(col)
			colHigh := math.Ceil(col)
			colViggle := col - colLow

			pixel := 0.0
			if rowLow > -1 && rowLow < w.rows {
				imgIndex := rowLow * w.cols
				if colLow > -1 && colLow < w.cols {
					pixel += w.img[int(imgIndex+colLow)] * (1 - rowViggle) * (1 - colViggle)
				}
				if colHigh > -1 && colHigh < w.cols {
					pixel += w.img[int(imgIndex+colHigh)] * (1 - rowViggle) * (colViggle)
				}
			}
			if rowHigh > -1 && rowHigh < w.rows {
				imgIndex := rowHigh * w.cols
				if colLow > -1 && colLow < w.cols {
					pixel += w.img[int(imgIndex+colLow)] * (rowViggle) * (1 - colViggle)
				}
				if colHigh > -1 && colHigh < w.cols {
					pixel += w.img[int(imgIndex+colHigh)] * (rowViggle) * (colViggle)
				}
			}

			w.view[viewIndex] = pixel

			viewIndex++
		}
	}
}
