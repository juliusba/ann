package mnist

import (
	"fmt"
	"math"
	"testing"
	"vecmat"
)

func initWalker() (w *Walk) {
	img := []float64{
		1, 2, 3, 4, 5,
		6, 7, 8, 9, 10,
		11, 12, 13, 14, 15,
		16, 17, 18, 19, 20,
		21, 22, 23, 24, 25,
	}
	w = NewWalk(img, 5, 5, 1.5)
	return
}

func TestNewWalker(t *testing.T) {
	w := initWalker()
	view := w.View()
	fasit := []float64{
		7, 8, 9,
		12, 13, 14,
		17, 18, 19,
	}
	for i := range view {
		if math.Abs(view[i]-fasit[i]) > 0.0001 {
			t.Fail()
			fmt.Println(vecmat.NewMatrix(view, 3, 3))
			break
		}
	}
}

func TestWalk(t *testing.T) {
	w := initWalker()
	w.Walk(1.0, -1.0)

	view := w.View()
	fasit := []float64{
		11, 12, 13,
		16, 17, 18,
		21, 22, 23,
	}
	for i := range view {
		if math.Abs(view[i]-fasit[i]) > 0.0001 {
			t.Fail()
			fmt.Println(vecmat.NewMatrix(view, 3, 3))
			break
		}
	}
}

func TestTurn(t *testing.T) {
	w := initWalker()

	w.Turn(math.Pi / 2)
	view := w.View()
	fasit := []float64{
		9, 14, 19,
		8, 13, 18,
		7, 12, 17,
	}
	for i := range view {
		if math.Abs(view[i]-fasit[i]) > 0.0001 {
			t.Fail()
			fmt.Println(vecmat.NewMatrix(view, 3, 3))
			break
		}
	}
}

func TestTurnAndWalk(t *testing.T) {
	w := initWalker()
	w.Turn(math.Pi / 2)
	w.Walk(1.0, -1.0)

	view := w.View()
	fasit := []float64{
		3, 8, 13,
		2, 7, 12,
		1, 6, 11,
	}
	for i := range view {
		if math.Abs(view[i]-fasit[i]) > 0.0001 {
			t.Fail()
			fmt.Println(view[i], fasit[i])
			fmt.Println(vecmat.NewMatrix(view, 3, 3))
			break
		}
	}
}
