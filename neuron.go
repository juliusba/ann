package ann

type NeuronIniter interface {
	Init(neuron *Neuron)
}

type NeuronParamer interface {
	NeuronParams() []Param
}

type NeuronVarer interface {
	NeuronVars() map[NeuronKey]float64
}

type NeuronPrereqer interface {
	NeuronPrereqs() []NeuronKey
}

type NeuronUpdater interface {
	Update(neuron *Neuron)
}

type NeuronTrainer interface {
	Train(neuron *Neuron)
}

type Neuron struct {
	Id string

	Inputs  []*Connection
	outputs []*Connection

	SI      float64
	Val     float64
	History []float64

	tick int

	historyLen int

	meta map[NeuronKey]float64
}

func NewNeuron(id string) (n Neuron) {
	n.Id = id
	n.Inputs = make([]*Connection, 0, 10)
	n.outputs = make([]*Connection, 0, 10)
	n.History = make([]float64, 1, 100)
	n.meta = make(map[NeuronKey]float64)
	return
}

func (n *Neuron) Update() {
	n.tick++
	n.addCurrentStateToHistory()
}

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------- State --------------------------------------------------------
// -------------------------------------------------------------------------------------------------------

func (n *Neuron) State(tick int) float64 {
	if tick > n.tick {
		if tick == -1 {
			return n.Val
		}
		panic("Trying to get a neurons state before it is updated!")
	}
	return n.History[tick%len(n.History)]
}

func (n *Neuron) Change() (change float64) {
	change = n.Val - n.History[n.tick-1%len(n.History)]
	return
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------ Inputs / Outpust -------------------------------------------------
// -------------------------------------------------------------------------------------------------------

func (n *Neuron) AddInput(input *Connection) {
	if cap(n.Inputs) == len(n.Inputs) {
		temp := n.Inputs
		n.Inputs = make([]*Connection, len(n.Inputs), 2*len(n.Inputs))
		copy(n.Inputs, temp)
	}
	n.Inputs = append(n.Inputs, input)
}

func (n *Neuron) AddOutput(output *Connection) {
	if cap(n.outputs) == len(n.outputs) {
		temp := n.outputs
		n.outputs = make([]*Connection, len(n.outputs), 2*len(n.outputs))
		copy(n.outputs, temp)
	}
	n.outputs = append(n.outputs, output)
}

// -------------------------------------------------------------------------------------------------------
// ----------------------------------------- History -----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

func (n *Neuron) SetHistoryLen(historyLen int) {
	n.historyLen = historyLen
}

func (n *Neuron) BoundHistoryLen() {
	n.historyLen = len(n.History)
}

func (n *Neuron) addCurrentStateToHistory() {
	if n.historyLen != 0 {
		if len(n.History) < n.historyLen {
			n.History = append(n.History, n.Val)
		} else {
			n.History[n.tick%n.historyLen] = n.Val
		}
		return
	}

	if cap(n.History) == len(n.History) {
		temp := n.History
		n.History = make([]float64, len(n.History), 2*len(n.History))
		copy(n.History, temp)
	}

	n.History = append(n.History, n.Val)
}
