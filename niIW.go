package ann

import "math/rand"

// -------------------------------------------------------------------------------------------------------
// ----------------------------------------- Standard ----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type NI_IW_Standard struct{}

func NewNI_IW_Standard() (ni *NI_IW_Standard) {
	ni = new(NI_IW_Standard)
	return
}

func (_ *NI_IW_Standard) Init(n *Neuron) {
	for i := range n.Inputs {
		if weight, ok := n.Inputs[i].meta[KeyWeight]; ok {
			n.Inputs[i].Weight = weight
		} else {
			n.Inputs[i].Weight = rand.Float64()*2 - 1
		}
	}
}

func (_ *NI_IW_Standard) InputParams() (params []Param) {
	params = []Param{
		ParamsInput[KeyWeight],
	}
	return
}
