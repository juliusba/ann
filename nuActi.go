package ann

import (
	"math"
	"math/rand"
)

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------- Sigmoid ---------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type NU_Acti_Sigmoid struct{}

func NewNU_Acti_Sigmoid() (sig *NU_Acti_Sigmoid) {
	sig = new(NU_Acti_Sigmoid)
	return
}

func (nu *NU_Acti_Sigmoid) Update(n *Neuron) {
	var (
		tau  = n.meta[KeySigmoidTau]
		g    = n.meta[KeySigmoidG]
		bias = n.meta[KeySigmoidBias]
		y    = n.meta[KeySigmoidY]
	)

	y += (n.SI - y + bias) / tau
	n.Val = 1.0 / (1.0 + math.Exp(-g*y))

	n.meta[KeySigmoidY] = y
}

func (_ *NU_Acti_Sigmoid) NeuronParams() (params []Param) {
	params = []Param{
		ParamsNeuron[KeySigmoidTau],
		ParamsNeuron[KeySigmoidG],
		ParamsNeuron[KeySigmoidBias],
	}
	return
}

func (_ *NU_Acti_Sigmoid) NeuronVariables() (vars map[NeuronKey]float64) {
	vars = map[NeuronKey]float64{
		KeySigmoidY: 0.0,
	}
	return
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------ Probabilistic binary ---------------------------------------------
// -------------------------------------------------------------------------------------------------------

type NU_Acti_BinProb struct{}

func NewNU_Acti_BinProb() (nu *NU_Acti_BinProb) {
	nu = new(NU_Acti_BinProb)
	return
}

func (nu *NU_Acti_BinProb) Init(neuron *Neuron) {
	probability := neuron.meta[KeyProb]

	if _, ok := neuron.meta[KeyAdaptRateActive]; !ok {
		neuron.meta[KeyAdaptRateActive] = 1 / (2 * probability)
	}
	if _, ok := neuron.meta[KeyAdaptRateActive]; !ok {
		neuron.meta[KeyAdaptRateActive] = -1 / (2 * (probability - 1))
	}

	if _, ok := neuron.meta[KeyThreshold]; !ok {
		nullMean, ok1 := neuron.meta[KeyBinNullMean]
		nullSD, ok2 := neuron.meta[KeyBinNullSD]
		if !ok1 || !ok2 {
			nullMean = 0.0
			nullSD = 0.0
			for i := range neuron.Inputs {
				prob := neuron.meta[KeyProb]
				weight := neuron.Inputs[i].Weight
				nullMean += weight * prob
				nullSD += weight * prob * (1 - prob)
			}
			nullSD = math.Sqrt(nullSD)
		}
		neuron.meta[KeyThreshold] = nullMean + nullSD*1.28 // ztable.Z(probability) 10% for now.
	}
}

func (ab *NU_Acti_BinProb) Update(neuron *Neuron) {
	var (
		threshold  = neuron.meta[KeyThreshold]
		plasticity = neuron.meta[KeyPlasticity]
		siSD       = neuron.meta[KeySISD]
	)

	if neuron.SI > threshold {
		neuron.Val = 1.0
		// Increase threshold
		threshold += siSD * plasticity * neuron.meta[KeyAdaptRateActive]
	} else {
		neuron.Val = 0.0
		// Decrease threshold
		threshold -= siSD * plasticity * neuron.meta[KeyAdaptRateInactive]
	}
	neuron.meta[KeyThreshold] = threshold
}

func (nu *NU_Acti_BinProb) Train(neuron *Neuron, value float64) {
	var (
		threshold  = neuron.meta[KeyThreshold]
		plasticity = neuron.meta[KeyPlasticity]
		siSD       = neuron.meta[KeySISD]
	)
	neuron.Val = value
	if neuron.Val == 1.0 {
		threshold += siSD * plasticity * neuron.meta[KeyAdaptRateActive]
	} else {
		threshold -= siSD * plasticity * neuron.meta[KeyAdaptRateInactive]
	}
}

func (nu *NU_Acti_BinProb) NeuronParams() (params []Param) {
	params = []Param{
		ParamsNeuron[KeyPlasticity],
		ParamsNeuron[KeyProb],
	}
	return
}

func (_ *NU_Acti_BinProb) NeuronPrerequisites() (keys []NeuronKey) {
	keys = []NeuronKey{
		KeySISD,
	}
	return
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------ Binary -----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type NU_Acti_Bin struct{}

func NewNU_Acti_Bin() (nu *NU_Acti_Bin) {
	nu = new(NU_Acti_Bin)
	return
}

func (ab *NU_Acti_Bin) Update(neuron *Neuron) {
	var (
		min = neuron.meta[KeySIMin]
		max = neuron.meta[KeySIMax]
	)

	if rand.Float64() < (neuron.SI-min)/(max-min) {
		neuron.Val = 1.0
	} else {
		neuron.Val = 0.0
	}
}

func (_ *NU_Acti_Bin) NeuronPrerequisites() (keys []NeuronKey) {
	keys = []NeuronKey{
		KeySIMin,
		KeySIMax,
	}
	return
}
