package ann

import "errors"

type NU_Learn_BinMaxAmp struct{}

/* A pattern makes it possible to "fill in the blanks" so to speek.
If a portion of its inputs are active, then it should fire.
The information contained in the neuron is the amount of
information in the surpluss of active neurons, relative to the
the level of activity that can be concidered "normal" (average)*/
func NewNU_Learn_BinMaxAmp() (nu *NU_Learn_BinMaxAmp) {
	nu = new(NU_Learn_BinMaxAmp)
	return
}

func (nu *NU_Learn_BinMaxAmp) Init(neuron *Neuron) {
	_, activeOK := neuron.meta[KeyAdaptRateActive]
	_, inactiveOK := neuron.meta[KeyAdaptRateInactive]
	if !activeOK || !inactiveOK {
		panic(errors.New("(NU_Learn_BinMaxAmp) No adaptationRate is given!"))
	}
	for _, input := range neuron.Inputs {
		_, activeOK = input.axon.meta[KeyAdaptRateActive]
		_, inactiveOK = input.axon.meta[KeyAdaptRateInactive]
		if !activeOK || !inactiveOK {
			panic(errors.New("(NU_Learn_BinMaxAmp) No adaptationRate is given for one or more of the neurons inputs!"))
		}
	}
}

func (nu *NU_Learn_BinMaxAmp) Update(neuron *Neuron) {
	var plasticity = neuron.meta[KeyPlasticity]

	if neuron.Val == 1 {
		// Increase weights for all active neurons.
		for i := range neuron.Inputs {
			input := neuron.Inputs[i]
			if contributionMagnitude := input.axon.State(neuron.tick); contributionMagnitude == 1 {
				input.Weight += contributionMagnitude * plasticity *
					input.axon.meta[KeyAdaptRateActive] * neuron.meta[KeyAdaptRateActive]
			}
		}
	} else {
		// Decrease weights for all active neurons.
		for i := range neuron.Inputs {
			input := neuron.Inputs[i]
			if contributionMagnitude := input.axon.State(neuron.tick); contributionMagnitude == 1 {
				input.Weight -= contributionMagnitude * plasticity *
					input.axon.meta[KeyAdaptRateActive] * neuron.meta[KeyAdaptRateInactive]
			}
		}
	}
}

func (nu *NU_Learn_BinMaxAmp) Train(neuron *Neuron, value float64) {
	nu.Update(neuron)
}
