package ann

import (
	"math"
	"math/rand"
)

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------ Regular ----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type NU_SI_Regular struct{}

func NewNU_SI_Regular() (siReg *NU_SI_Regular) {
	siReg = new(NU_SI_Regular)
	return
}

func (_ *NU_SI_Regular) Update(neuron *Neuron) {
	neuron.SI = 0.0
	for _, input := range neuron.Inputs {
		neuron.SI += input.axon.State(neuron.tick) * input.Weight
	}
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------- Delay -----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type NU_SI_Delayer struct{}

func NewNU_SI_Delayer() (siDelayer *NU_SI_Delayer) {
	siDelayer = new(NU_SI_Delayer)
	return
}

func (siDelayer *NU_SI_Delayer) Update(neuron *Neuron) {
	var plasticity = neuron.meta[KeyPlasticity]

	neuron.SI = 0.0
	for _, input := range neuron.Inputs {
		var (
			inputLastActive = input.axon.meta[KeyLastActive]
			delay           = input.meta[KeySIdelay]
		)

		delay += ((inputLastActive - float64(neuron.tick)) - delay) * plasticity

		input.meta[KeySIdelay] = delay
		if rand.Float64() < delay-math.Floor(delay) {
			delay += 1.0
		}
		neuron.SI += input.axon.State(neuron.tick-int(delay)) * input.Weight
	}
}
