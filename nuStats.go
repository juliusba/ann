package ann

import "math"

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------- LastActive ---------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type NU_Stats_LastActive struct{}

func NewNU_Stats_LastActive() (nu *NU_Stats_LastActive) {
	nu = new(NU_Stats_LastActive)
	return
}

func (_ *NU_Stats_LastActive) Update(neuron *Neuron) {
	if neuron.History[neuron.tick%len(neuron.History)] > 0.5 {
		neuron.meta[KeyLastActive] = float64(neuron.tick)
	}
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------ Average ----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type NU_Stats_Average struct {
	tau float64
}

func NewNU_Stats_Average() (nu *NU_Stats_Average) {
	nu = new(NU_Stats_Average)
	nu.tau = 1
	return
}

func (nu *NU_Stats_Average) Update(neuron *Neuron) {
	var (
		avg      = neuron.meta[KeyAvg]
		variance = neuron.meta[KeyVar]
	)

	avg += (neuron.State(neuron.tick) - avg) / nu.tau
	variance += (math.Pow(neuron.State(neuron.tick)-avg, 2) - variance) / nu.tau

	neuron.meta[KeyAvg] = avg
	neuron.meta[KeyVar] = variance
}

func (nu *NU_Stats_Average) Tick(tick float64) {
	nu.tau += 1 / math.Sqrt(tick)
}

// -------------------------------------------------------------------------------------------------------
// -------------------------------------------- SI -------------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type NU_Stats_SI struct {
	tau float64
}

func NewNU_Stats_SI() (nu *NU_Stats_SI) {
	nu = new(NU_Stats_SI)
	nu.tau = 1
	return
}

func (nu *NU_Stats_SI) Update(neuron *Neuron) {
	var (
		siMean = neuron.meta[KeySIMean]
		siVar  = neuron.meta[KeySIVar]
	)

	neuron.meta[KeySIVar] = siVar + (math.Pow(neuron.SI-siMean, 2)-siVar)/nu.tau
	neuron.meta[KeySIMean] = siMean + (neuron.SI-siMean)/nu.tau
	neuron.meta[KeySISD] = math.Sqrt(siVar)
	if min := neuron.meta[KeySIMin]; neuron.SI < min {
		neuron.meta[KeySIMin] = neuron.SI
	} else if max := neuron.meta[KeySIMax]; neuron.SI > max {
		neuron.meta[KeySIMax] = neuron.SI
	}
}

func (nu *NU_Stats_SI) Tick(tick float64) {
	nu.tau += 1 / math.Sqrt(tick)
}

func (_ *NU_Stats_SI) NeuronVariables() (vars map[NeuronKey]float64) {
	vars = map[NeuronKey]float64{
		KeySIMean: 0.0,
		KeySIVar:  0.0,
		KeySISD:   0.0,
		KeySIMin:  math.MaxFloat64,
		KeySIMax:  -math.MaxFloat64,
	}
	return
}

// -------------------------------------------------------------------------------------------------------
// ----------------------------------------- Binary SI ---------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type NU_Stats_BinSI struct{}

func NewNU_Stats_BinSI() (nu *NU_Stats_BinSI) {
	nu = new(NU_Stats_BinSI)
	return
}

func (nu *NU_Stats_BinSI) Init(neuron *Neuron) {
	nu.Update(neuron)
}

func (nu *NU_Stats_BinSI) Update(neuron *Neuron) {
	nullMean, nullSD := 0.0, 0.0
	for i := range neuron.Inputs {
		var (
			probability = neuron.Inputs[i].axon.meta[KeyProb]
		)

		weight := neuron.Inputs[i].Weight
		nullMean += probability * weight
		nullSD += probability * (1 - probability) * weight
	}
	nullSD = math.Sqrt(nullSD)

	neuron.meta[KeyBinNullMean] = nullMean
	neuron.meta[KeyBinNullSD] = nullSD
}

func (_ *NU_Stats_BinSI) NeuronVariables() (vars map[NeuronKey]float64) {
	vars = map[NeuronKey]float64{
		KeyBinNullMean: 0.0,
		KeyBinNullSD:   0.0,
	}
	return
}
