package ann

import "vecea"

type Param struct {
	Key  interface{}
	Name string

	Default  float64
	Min, Max float64
	Exp      bool
}

func (p *Param) BP() (bp vecea.BlueprintElement) {
	if p.Exp {
		bp = vecea.NewBlueprintElementExp(p.Min, p.Max, p.Name)
	} else {
		bp = vecea.NewBlueprintElement(p.Min, p.Max, p.Name)
	}
	return
}

func (one *Param) Equals(other *Param) bool {
	return one.Min == other.Min && one.Max == other.Max &&
		one.Name == other.Name && one.Exp == other.Exp
}

// -------------------------------------------------------------------------------------------------------
// ------------------------------------------ Neuron -----------------------------------------------------
// -------------------------------------------------------------------------------------------------------

//go:generate stringer -type=NeuronKey

type NeuronKey uint8

const (
	KeyThreshold  NeuronKey = iota
	KeyPlasticity NeuronKey = iota
	KeyProb       NeuronKey = iota

	KeyAvg        NeuronKey = iota
	KeyVar        NeuronKey = iota
	KeyLastActive NeuronKey = iota

	KeySIMean NeuronKey = iota
	KeySIVar  NeuronKey = iota
	KeySISD   NeuronKey = iota
	KeySIMin  NeuronKey = iota
	KeySIMax  NeuronKey = iota

	KeyAdaptRateActive   NeuronKey = iota
	KeyAdaptRateInactive NeuronKey = iota

	KeyBinNullMean NeuronKey = iota
	KeyBinNullSD   NeuronKey = iota

	KeySigmoidTau  NeuronKey = iota
	KeySigmoidG    NeuronKey = iota
	KeySigmoidBias NeuronKey = iota
	KeySigmoidY    NeuronKey = iota

	KeyFirstUnusedKey NeuronKey = iota
)

var (
	ParamsNeuron = map[NeuronKey]Param{
		KeyPlasticity: Param{
			Key:     KeyPlasticity,
			Default: 0.05,
			Min:     0.0001,
			Max:     1.0,
			Exp:     true,
			Name:    "Plasticity",
		},
		KeyProb: Param{
			Key:     KeyProb,
			Default: 0.1,
			Min:     0.001,
			Max:     0.5,
			Exp:     true,
			Name:    "Probability",
		},

		// ----------------------------------
		// ------------ Sigmoid -------------
		// ----------------------------------
		KeySigmoidTau: Param{
			Key:     KeySigmoidTau,
			Default: 1.0,
			Min:     1.0,
			Max:     5.0,
			Exp:     true,
			Name:    "SigmoidTau",
		},
		KeySigmoidG: Param{
			Key:     KeySigmoidG,
			Default: 1.0,
			Min:     1.0,
			Max:     10.0,
			Exp:     true,
			Name:    "SigmoidG",
		},
		KeySigmoidBias: Param{
			Key:  KeySigmoidBias,
			Min:  -3.0,
			Max:  3.0,
			Name: "SigmoidBias",
		},
	}
)

// -------------------------------------------------------------------------------------------------------
// ---------------------------------------- Connection ---------------------------------------------------
// -------------------------------------------------------------------------------------------------------

type InputKey uint8

const (
	KeySIdelay InputKey = iota
	KeyWeight  InputKey = iota
)

var (
	ParamsInput = map[InputKey]Param{
		KeyWeight: Param{
			Key:  KeyWeight,
			Min:  -1.0,
			Max:  1.0,
			Name: "Weight",
		},
	}
)
