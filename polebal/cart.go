package polebal

import "math"

type actor interface {
	Tick(input []float64) (output []float64)
}

type Pole struct {
	AngVelocity float64
	Angle       float64

	Length   float64
	Mass     float64
	Friction float64
}

type Cart struct {
	pole       Pole
	secondPole Pole

	Velocity float64
	Position float64

	Mass      float64
	TotalMass float64
	Length    float64
	Friction  float64

	Force    float64
	MaxForce float64

	sign_v float64

	history []historyElement
}

type historyElement struct {
	Angle    float64 `json:"a"`
	Angle2   float64 `json:"a2"`
	Position float64 `json:"p"`
	MaxForce float64 `json:"f"`
}

func (c *Cart) Test(a actor, durationInSeconds float64, secondsPerTick float64, twoPoles bool) (secondsBeforeFail float64) {
	for t := 0.0; t < durationInSeconds; t += secondsPerTick {
		input := []float64{
			c.pole.Angle / _poleFailureAngle, // angleInput
			c.Position / _trackLimit,         // positionInput
			c.pole.AngVelocity / 0.5,         // angVelInput
			c.Velocity / 0.5,                 // velInput
		}

		if twoPoles {
			input = append(input, []float64{
				c.secondPole.Angle / _poleFailureAngle,
				c.secondPole.AngVelocity / 0.5,
			}...)
		}

		Output := a.Tick(input)
		var F float64
		if dir := Output[0] - Output[1]; dir > 0.2 {
			F = c.MaxForce
		} else if dir < -0.2 {
			F = -c.MaxForce
		} else {
			F = 0.0
		}
		c.tick(F, secondsPerTick, twoPoles)

		if (twoPoles && math.Abs(c.secondPole.Angle) > _poleFailureAngle) ||
			math.Abs(c.pole.Angle) > _poleFailureAngle ||
			math.Abs(c.Position) > _trackLimit {
			return t
		}

	}

	return durationInSeconds
}

func (c *Cart) tick(F float64, seconds float64, twoPoles bool) {
	if c.Velocity > 0 {
		c.sign_v = 1
	} else if c.Velocity > 0 {
		c.sign_v = -1
	} else {
		c.sign_v = 0
	}

	d_w1, F1 := c.tickPole(F, false)
	c.pole.Angle += c.pole.AngVelocity * seconds
	c.pole.AngVelocity += d_w1 * seconds
	F += F1

	if twoPoles {
		d_w2, F2 := c.tickPole(F, true)
		c.secondPole.Angle += c.secondPole.AngVelocity * seconds
		c.secondPole.AngVelocity += d_w2 * seconds
		F += F2
	}
	c.Force = F

	a := F / c.TotalMass

	c.Position += c.Velocity * seconds
	c.Velocity += a * seconds

	c.history = append(c.history, historyElement{c.pole.Angle, c.secondPole.Angle, c.Position, F})
}

func (c *Cart) tickPole(F float64, second bool) (d_w, F_pole float64) {
	var m_p, l, ang, w float64

	if !second {
		m_p = c.pole.Mass
		// mu_p = c.pole.Friction
		l = c.pole.Length
		ang = c.pole.Angle
		w = c.pole.AngVelocity
	} else {
		m_p = c.secondPole.Mass
		// mu_p = c.secondPole.Friction
		l = c.secondPole.Length
		ang = c.secondPole.Angle
		w = c.secondPole.AngVelocity
	}
	sin_ang := math.Sin(ang)
	cos_ang := math.Cos(ang)

	// poleFriction := (mu_p * w) / (c.Mass * l)
	g_F := _g * sin_ang
	frac1 := cos_ang * ((c.Friction * c.sign_v) - c.Force - (m_p * l * w * w * sin_ang)) / c.TotalMass
	// frac1 := cos_ang * (-F - (m_p * l * w * w * sin_ang)) / c.TotalMass
	frac2 := l * (4.0/3.0 - m_p/c.TotalMass*cos_ang*cos_ang)

	d_w = (g_F + frac1) / frac2
	F_pole = m_p * l * (w*w*sin_ang - d_w*cos_ang)
	return
}
