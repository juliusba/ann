package polebal

import (
	"log"
	"math"
	"vecea"
)

type LapTester struct{}

func NewLapTester() (tester *LapTester) {
	tester = new(LapTester)
	return
}

func (tester *LapTester) TestIndivid(ea *vecea.EA, individ *vecea.Individ, phenotype interface{}) (score float64) {
	if poleHistory, ok := individ.Meta("poleHistory").([]historyElement); ok {
		resets := individ.Meta("poleResets").([]int)
		resetIndex := 0
		leftFlag, rightFlag := -0.8*_trackLimit, 0.8*_trackLimit
		right := false
		laps := 0.0
		for i, el := range poleHistory {
			if i == resets[resetIndex]-1 {
				resetIndex++
				right = false
				if right == true {
					score += math.Abs(rightFlag-poleHistory[len(poleHistory)-1].Position) / (4.0 * _trackLimit)
				} else {
					score += math.Abs(leftFlag-poleHistory[len(poleHistory)-1].Position) / (4.0 * _trackLimit)
				}
			}

			if right {
				if el.Position >= rightFlag {
					right = false
					laps++
				}
			} else {
				if el.Position <= leftFlag {
					right = true
					laps++
				}
			}
		}
		score += laps

		if right == true {
			score += math.Abs(rightFlag-poleHistory[len(poleHistory)-1].Position) / (4.0 * _trackLimit)
		} else {
			score += math.Abs(leftFlag-poleHistory[len(poleHistory)-1].Position) / (2.0 * _trackLimit)
		}

		individ.SetMeta("laps", laps, true)
		individ.SetMeta("lapScore", score, false)

	} else {
		log.Fatal("ERROR (polebal.LapTester.TestIndivid): No poleHistory found")
	}

	return
}

func (tester *LapTester) TestName() (name string) {
	return "LapTester"
}

func (tester *LapTester) String() string {
	return "(Tester)\t Laps"
}
