package polebal

import (
	"ann"
	"fmt"
	"log"
	"math"
	"math/rand"
	"vecea"
)

var (
	_duration         = 600.0 // seconds
	_testDuration     = 60.0  // seconds
	_tests            = 200   // nr of tests
	_secondsPerTick   = 0.02  // 50 Hertz
	_m_c              = 1.0   // Kg
	_m_p              = 0.1   // Kg
	_m_p2             = 0.04  // Kg
	_mu_c             = 0.0   // Friction on ground
	_mu_p             = 0.0   // Friction on pole pivot
	_mu_p2            = 0.0   // Friction on second pole pivot
	_trackLimit       = 2.4   // Meters
	_poleFailureAngle = 0.209 // Radians
	_l                = 0.5   // Meters (distance from pivot to center of mass)
	_l2               = 0.2   // Meters (distance from second pivot to center of mass)
	_g                = 9.81  // Meters/Second^2
	_F                = 1.0   // Newton
)

type PoleBalancingTest struct {
	duration       float64
	testDuration   float64
	tests          int
	secondsPerTick float64
	twoPoles       bool

	cart Cart
}

func StandardPoleBalancingTest(poleCount int, force float64) (test *PoleBalancingTest, inputs, outputs int) {
	test = new(PoleBalancingTest)
	test.duration = _duration
	test.tests = _tests
	test.testDuration = _testDuration
	test.secondsPerTick = _secondsPerTick
	test.cart.Mass = _m_c
	test.cart.TotalMass = _m_c + _m_p
	test.cart.pole.Mass = _m_p
	test.cart.Friction = _mu_c
	test.cart.pole.Friction = _mu_p
	test.cart.pole.Length = _l
	test.cart.MaxForce = force
	test.twoPoles = poleCount == 2

	if test.twoPoles {
		inputs = 6
		test.cart.secondPole.Mass = _m_p2
		test.cart.TotalMass += _m_p2
		test.cart.secondPole.Friction = _mu_p2
		test.cart.secondPole.Length = _l2
	} else {
		inputs = 4
	}
	outputs = 2

	return
}

func (p *PoleBalancingTest) TestIndivid(individ *vecea.Individ, phenotype interface{}) (score float64) {
	actor := phenotype.(actor)

	totalDuration := 0.0
	fails := 0.0
	cart := p.cart
	resets := make([]int, int(p.duration/2))
	for i := 0; i < p.tests && totalDuration < p.duration; i++ {
		cart.Velocity = 0
		cart.Position = 0
		cart.pole.AngVelocity = 0
		cart.pole.Angle = rand.NormFloat64() * 0.01
		if p.twoPoles {
			cart.secondPole.AngVelocity = 0
			cart.secondPole.Angle = rand.NormFloat64() * 0.01
		}
		testDuration := math.Min(p.testDuration, p.duration-totalDuration)
		duration := cart.Test(actor, testDuration, p.secondsPerTick, p.twoPoles)
		if duration < testDuration {
			fails++
		}
		totalDuration += duration
		resets = append(resets, len(cart.history))
	}

	individ.SetMeta("poleHistory", cart.history, false)
	individ.SetMeta("poleResets", resets, false)
	individ.SetMeta("poleFails", fails, false)

	score = fails * p.duration / totalDuration
	return
}

func (p *PoleBalancingTest) TestName() (name string) {
	return "poleBalancing"
}

func (p *PoleBalancingTest) IsFinished(individ *vecea.Individ) (isFinished bool) {
	return individ.TestScore(p.TestName()) == 0.0
}

func (p *PoleBalancingTest) String() string {
	poles := 1
	if p.twoPoles {
		poles = 2
	}
	return fmt.Sprintf("(Tester)\t Polebalancing (Poles: %v, Duration: %v, TestDuration: %v, Hertz: %v, Force %v)",
		poles, p.duration, p.testDuration, 1.0/p.secondsPerTick, p.cart.Force)
}

func (p *PoleBalancingTest) Init(ea *vecea.EA) {
	if bp, ok := ea.Blueprinter.(*ann.AnnI); ok {
		if bp.Outputs() != 2 {
			log.Fatalf("ERROR (polebal): actors must have 2 outputs (has %v)!", bp.Outputs())
		}
		if p.twoPoles {
			if bp.Inputs() != 6 {
				log.Fatalf("ERROR (polebal): actors must have 6 inputs (has %v)!", bp.Inputs())
			}
		} else {
			if bp.Inputs() != 4 {
				log.Fatalf("ERROR (polebal): actors must have 4 inputs (has %v)!", bp.Inputs())
			}
		}
	} else {
		log.Fatal("ERROR (polebal): Blueprinter must produce actors!")
	}
}

func (p *PoleBalancingTest) MetaData(ea *vecea.EA, individ *vecea.Individ, metaData map[string]interface{}) {
	ann := ea.Phenotype(individ)
	tempT := p.tests
	tempD := p.duration
	p.tests = 5
	p.duration = 120
	p.TestIndivid(individ, ann)
	p.tests = tempT
	p.duration = tempD

	metaData["ann"] = ann
	metaData["poleHistory"] = individ.Meta("poleHistory")
}
