package polebal

import (
	"ann"
	"fmt"
	"runtime"
	"testing"
	"vecea"
)

func TestPoleBalancing(t *testing.T) {
	p, inputs, outputs := StandardPoleBalancingTest(1, 1)
	anni := ann.NewAnnISigmoid(inputs, outputs, []int{3, 2}, ann.Connectivity.FullyConnected)
	individ := vecea.NewIndivid(anni.Blueprint())
	individ.Randomize()
	ann := anni.Phenotype(individ)
	p.TestIndivid(individ, ann)
	fmt.Println("Fails:", 1.0/individ.Fitness(), "Fitness:", individ.Fitness())
}

func TestPoleBalancingEA(t *testing.T) {
	runtime.GOMAXPROCS(runtime.NumCPU())
	p, inputs, outputs := StandardPoleBalancingTest(2, 3)
	anni := ann.NewAnnISigmoid(inputs, outputs, []int{}, ann.Connectivity.FullyConnected)

	ea := vecea.NewEAAdaDiff(100, anni)

	ea.AddTesters(p)
	// ea.AddTesters(ann.NewAnnTesterNeuronSignificance(), 0.1)
	// ea.AddTesters(NewLapTester(), 0.2)
	ea.Init()

	done := false
	for i := 0; i < 1000 && !done; i++ {
		done = ea.RunGenerationStep()
		var memStats runtime.MemStats
		runtime.ReadMemStats(&memStats)
		fmt.Printf("\rGen: %v, Fitness: %v, Fails: %v, Laps: %v, LapScore: %v, Memory: %v, Sys: %v, PopulationSize: %v, Age: %v         ",
			i, ea.IndividBest().Fitness(), ea.IndividBest().Meta("poleFails"), ea.IndividBest().Meta("laps"),
			ea.IndividBest().Meta("lapScore"), memStats.Alloc/(1024*1024), memStats.Sys/(1024*1024), len(ea.Population), ea.StatsAge().Max)
	}
}
