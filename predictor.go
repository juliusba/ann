package ann

// nX = Predictor. nY = Predictand.
// type Predi struct {
// 	nX, nY *Neuron

// 	samples, delay int

// 	pXY float64
// 	hXY float64

// 	gain        float64
// 	information float64
// }

// func (this *Predi) calcInfo() float64 {
// 	this.hXY = this.pXY*math.Log2(this.pXY) + (1-this.pXY)*math.Log2(1-this.pXY)
// 	this.gain = this.nX.entropy - (this.hXY - this.nY.entropy)
// 	this.information = this.gain * this.nY.Information()

// 	return this.information
// }

// func (this *Predi) tick(currentTick int) {
// 	this.pXY += (this.nX.Output()*this.nY.Output() - this.pXY) / CONST_HISTORY_LEN

// 	this.calcInfo()
// }

// func (this *Predi) update(currentTick, tickCount int) float64 {
// 	pXY := 0.0
// 	for end := (currentTick - tickCount) % CONST_HISTORY_LEN; currentTick > end; {
// 		pXY += this.nX.HistoryPoint(currentTick, 1) * this.nY.HistoryPoint(currentTick, 1)

// 		currentTick--
// 		if currentTick == -1 {
// 			currentTick = CONST_HISTORY_LEN - 1
// 		}
// 	}

// 	this.pXY += (pXY - float64(tickCount)*this.pXY) / CONST_HISTORY_LEN

// 	return this.calcInfo()
// }

// // nX = The neuron that predicts. nY = The neuron that is being predicted.
// // Tradeoff: Computational cost vs accuracy. Current priority: Accuracy.
// // Coverage = P(this|other) NOT USED
// // Accuracy = P(other|this) NOT USED
// func RelativeInformationGain(nX, nY *Neuron, currentTick, delay int) *Predi {
// 	predi := new(Predi)

// 	predi.nX = nX
// 	predi.nY = nY

// 	for iX, iY, end := currentTick, (currentTick-delay)%CONST_HISTORY_LEN, (currentTick+1)%CONST_HISTORY_LEN; iX != end; {

// 		x, y := nX.HistoryPoint(iX, 1), nY.HistoryPoint(iY, 1)

// 		predi.pXY += x * y

// 		iX--
// 		iY--
// 		if iX == -1 {
// 			iX = CONST_HISTORY_LEN - 1
// 		} else if iY == -1 {
// 			iY = CONST_HISTORY_LEN - 1
// 		}
// 	}

// 	predi.delay = delay

// 	predi.pXY /= CONST_HISTORY_LEN

// 	predi.calcInfo()

// 	predi.nX.addPredictand(predi)
// 	predi.nY.addPredictor(predi)

// 	return predi
// }

// func (this *Predi) Prediction(currentTick int) float64 {
// 	return this.nX.HistoryPoint((currentTick-this.delay)%CONST_HISTORY_LEN, 1) * this.pXY * this.nY.avg / this.nX.avg
// }
