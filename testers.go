package ann

import (
	"errors"
	"fmt"
	"math"
	"vecmat"
)

// -------------------------------------------------------------------------------------------------------
// -------------- Have all neurons visited values both close to zero, and close to one? ------------------
// -------------------------------------------------------------------------------------------------------

type AnnTesterNeuronSignificance struct{}

func NewAnnTesterNeuronSignificance() (tester *AnnTesterNeuronSignificance) {
	tester = new(AnnTesterNeuronSignificance)
	return
}

func (tester *AnnTesterNeuronSignificance) TestSolution(solution interface{}) (score float64) {
	ann := solution.(*Ann)

	var (
		lowest, highest float64
		neurons         int
	)

	for l := 0; l < len(ann.Hidden); l++ {
		layer := ann.Hidden[l]
		neurons += len(layer.Neurons)
		for n := 0; n < len(layer.Neurons); n++ {
			lowest, highest = math.MaxFloat64, -math.MaxFloat64
			history := layer.Neurons[n].History
			for h := 0; h < len(history); h++ {
				historyVal := history[h]
				highest = math.Max(highest, historyVal)
				lowest = math.Min(lowest, historyVal)
			}
			score += (1 - lowest) * highest
		}
	}

	neurons += len(ann.Output.Neurons)
	for n := 0; n < len(ann.Output.Neurons); n++ {
		lowest, highest = math.MaxFloat64, -math.MaxFloat64
		history := ann.Output.Neurons[n].History
		for h := 0; h < len(history); h++ {
			historyVal := history[h]
			highest = math.Max(highest, historyVal)
			lowest = math.Min(lowest, historyVal)
		}
		score += (1 - lowest) * highest
	}

	score /= float64(neurons)

	return
}

func (tester *AnnTesterNeuronSignificance) TestName() (name string) {
	return "neuronSignificance"
}

func (tester *AnnTesterNeuronSignificance) TestSpecs() (minimize, logarithmic bool) {
	minimize = false
	logarithmic = false
	return
}

func (tester *AnnTesterNeuronSignificance) TestGoal() (goal float64, hasGoal bool) {
	return
}

func (tester *AnnTesterNeuronSignificance) String() string {
	return fmt.Sprintf("(Tester)\t NeuronSignificance")
}

// -------------------------------------------------------------------------------------------------------
// -------------- Have all neurons visited values both close to zero, and close to one? ------------------
// -------------------------------------------------------------------------------------------------------

type AnnTesterNeuronSignificanceN struct {
	N int
}

func NewAnnTesterNeuronSignificanceN(n int) (tester *AnnTesterNeuronSignificanceN) {
	tester = new(AnnTesterNeuronSignificanceN)
	tester.N = n
	return
}

func (tester *AnnTesterNeuronSignificanceN) TestSolution(solution interface{}) (score float64) {
	ann := solution.(*Ann)

	var (
		neurons int
	)

	nthLowHigh := func(history []float64) (nthLow, nthHigh float64) {
		if len(history) < tester.N {
			panic(errors.New("Neurons history is to short"))
		}
		lowest := vecmat.XVec(tester.N, math.MaxFloat64)
		highest := vecmat.XVec(tester.N, -math.MaxFloat64)

		for _, historyVal := range history {
			index := -1
			for i := tester.N - 1; i >= 0 && historyVal < lowest[i]; i-- {
				index = i
			}
			if index != -1 {
				lowest = append(lowest[:index], append([]float64{historyVal}, lowest[index:len(lowest)-1]...)...)
			}
			index = -1
			for i := tester.N - 1; i >= 0 && historyVal > highest[i]; i-- {
				index = i
			}
			if index != -1 {
				highest = append(highest[:index], append([]float64{historyVal}, highest[index:len(highest)-1]...)...)
			}
		}

		nthLow = lowest[tester.N-1]
		nthHigh = highest[tester.N-1]
		return
	}

	for l := 0; l < len(ann.Hidden); l++ {
		layer := ann.Hidden[l]
		neurons += len(layer.Neurons)
		for n := 0; n < len(layer.Neurons); n++ {
			nthLow, nthHigh := nthLowHigh(ann.Hidden[l].Neurons[n].History)
			score += (1 - nthLow) * nthHigh
		}
	}

	neurons += len(ann.Output.Neurons)
	for n := 0; n < len(ann.Output.Neurons); n++ {
		nthLow, nthHigh := nthLowHigh(ann.Output.Neurons[n].History)
		score += (1 - nthLow) * nthHigh
	}

	score /= float64(neurons)

	return
}

func (tester *AnnTesterNeuronSignificanceN) TestName() (name string) {
	return "neuronSignificanceN"
}

func (tester *AnnTesterNeuronSignificanceN) String() string {
	return fmt.Sprintf("(Tester)\t NeuronSignificanceN (N: %v)", tester.N)
}
